package com.scoreboard.util;

import com.scoreboard.pojo.nhl.BoxScore;
import com.scoreboard.pojo.nhl.BoxScoreTeam;
import com.scoreboard.pojo.nhl.BoxScoreTeams;
import com.scoreboard.pojo.nhl.LineScore;
import com.scoreboard.pojo.nhl.LineScoreTeam;
import com.scoreboard.pojo.nhl.LineScoreTeams;
import com.scoreboard.pojo.nhl.LiveData;
import com.scoreboard.pojo.nhl.LiveFeedResponse;
import com.scoreboard.pojo.nhl.StatType;
import com.scoreboard.pojo.nhl.Team;
import com.scoreboard.pojo.nhl.TeamSkaterStats;
import com.scoreboard.pojo.nhl.TeamStats;
import com.scoreboard.pojo.tile.Tile;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TileUtilTest {
    private static final String AWAY_ABBREVIATION = "CHI";
    private static final String HOME_ABBREVIATION = "STL";
    private static final int AWAY_GOALS = 1;
    private static final int HOME_GOALS = 4;
    private static final int AWAY_SHOTS = 15;
    private static final int HOME_SHOTS = 50;
    private static final int AWAY_PIM = 2;
    private static final int HOME_PIM = 6;
    private static final int AWAY_HITS = 8;
    private static final int HOME_HITS = 18;
    private static final String AWAY_POWER_PLAY_PERCENTAGE = "0.0%";
    private static final String HOME_POWER_PLAY_PERCENTAGE = "100.0%";
    private static final int AWAY_POWER_PLAY_GOALS = 0;
    private static final int HOME_POWER_PLAY_GOALS = 1;
    private static final int AWAY_POWER_PLAY_OPPORTUNITIES = 3;
    private static final int HOME_POWER_PLAY_OPPORTUNITIES = 1;
    private static final String AWAY_FACEOFF_PERCENTAGE = "40%";
    private static final String HOME_FACEOFF_PERCENTAGE = "60%";
    private static final int AWAY_BLOCKED_SHOTS = 13;
    private static final int HOME_BLOCKED_SHOTS = 20;
    private static final int AWAY_TAKE_AWAYS = 7;
    private static final int HOME_TAKE_AWAYS = 10;
    private static final int AWAY_GIVE_AWAYS = 10;
    private static final int HOME_GIVE_AWAYS = 3;
    private static final int PERIOD = 3;

    private LiveFeedResponse liveFeed;

    @Before
    public void setUp() throws Exception {
        liveFeed = mock(LiveFeedResponse.class);

        LiveData liveData = mock(LiveData.class);
        when(liveFeed.getLiveData()).thenReturn(liveData);

        BoxScore boxScore = mock(BoxScore.class);
        when(liveData.getBoxScore()).thenReturn(boxScore);

        BoxScoreTeams boxScoreTeams = mock(BoxScoreTeams.class);
        when(boxScore.getTeams()).thenReturn(boxScoreTeams);

        BoxScoreTeam awayBoxScoreTeam = mock(BoxScoreTeam.class);
        when(boxScoreTeams.getAway()).thenReturn(awayBoxScoreTeam);

        BoxScoreTeam homeBoxScoreTeam = mock(BoxScoreTeam.class);
        when(boxScoreTeams.getHome()).thenReturn(homeBoxScoreTeam);

        Team awayTeam = mock(Team.class);
        when(awayBoxScoreTeam.getTeam()).thenReturn(awayTeam);
        when(awayTeam.getAbbreviation()).thenReturn(AWAY_ABBREVIATION);

        Team homeTeam = mock(Team.class);
        when(homeBoxScoreTeam.getTeam()).thenReturn(homeTeam);
        when(homeTeam.getAbbreviation()).thenReturn(HOME_ABBREVIATION);

        TeamStats awayTeamStats = mock(TeamStats.class);
        when(awayBoxScoreTeam.getTeamStats()).thenReturn(awayTeamStats);

        TeamSkaterStats awayTeamSkaterStats = mock(TeamSkaterStats.class);
        when(awayTeamStats.getTeamSkaterStats()).thenReturn(awayTeamSkaterStats);
        when(awayTeamSkaterStats.getShots()).thenReturn(AWAY_SHOTS);
        when(awayTeamSkaterStats.getPim()).thenReturn(AWAY_PIM);
        when(awayTeamSkaterStats.getHits()).thenReturn(AWAY_HITS);
        when(awayTeamSkaterStats.getPowerPlayPercentage()).thenReturn(AWAY_POWER_PLAY_PERCENTAGE);
        when(awayTeamSkaterStats.getPowerPlayGoals()).thenReturn(AWAY_POWER_PLAY_GOALS);
        when(awayTeamSkaterStats.getPowerPlayOpportunities()).thenReturn(AWAY_POWER_PLAY_OPPORTUNITIES);
        when(awayTeamSkaterStats.getFaceOffWinPercentage()).thenReturn(AWAY_FACEOFF_PERCENTAGE);
        when(awayTeamSkaterStats.getBlocked()).thenReturn(AWAY_BLOCKED_SHOTS);
        when(awayTeamSkaterStats.getTakeaways()).thenReturn(AWAY_TAKE_AWAYS);
        when(awayTeamSkaterStats.getGiveaways()).thenReturn(AWAY_GIVE_AWAYS);

        TeamStats homeTeamStats = mock(TeamStats.class);
        when(homeBoxScoreTeam.getTeamStats()).thenReturn(homeTeamStats);

        TeamSkaterStats homeTeamSkaterStats = mock(TeamSkaterStats.class);
        when(homeTeamStats.getTeamSkaterStats()).thenReturn(homeTeamSkaterStats);
        when(homeTeamSkaterStats.getShots()).thenReturn(HOME_SHOTS);
        when(homeTeamSkaterStats.getPim()).thenReturn(HOME_PIM);
        when(homeTeamSkaterStats.getHits()).thenReturn(HOME_HITS);
        when(homeTeamSkaterStats.getPowerPlayPercentage()).thenReturn(HOME_POWER_PLAY_PERCENTAGE);
        when(homeTeamSkaterStats.getPowerPlayGoals()).thenReturn(HOME_POWER_PLAY_GOALS);
        when(homeTeamSkaterStats.getPowerPlayOpportunities()).thenReturn(HOME_POWER_PLAY_OPPORTUNITIES);
        when(homeTeamSkaterStats.getFaceOffWinPercentage()).thenReturn(HOME_FACEOFF_PERCENTAGE);
        when(homeTeamSkaterStats.getBlocked()).thenReturn(HOME_BLOCKED_SHOTS);
        when(homeTeamSkaterStats.getTakeaways()).thenReturn(HOME_TAKE_AWAYS);
        when(homeTeamSkaterStats.getGiveaways()).thenReturn(HOME_GIVE_AWAYS);


        LineScore lineScore = mock(LineScore.class);
        when(liveData.getLineScore()).thenReturn(lineScore);
        when(lineScore.getCurrentPeriod()).thenReturn(PERIOD);

        LineScoreTeams lineScoreTeams = mock(LineScoreTeams.class);
        when(lineScore.getTeams()).thenReturn(lineScoreTeams);

        LineScoreTeam awayLineScoreTeam = mock(LineScoreTeam.class);
        when(lineScoreTeams.getAwayTeamScore()).thenReturn(awayLineScoreTeam);
        when(awayLineScoreTeam.getGoals()).thenReturn(AWAY_GOALS);

        LineScoreTeam homeLineScoreTeam = mock(LineScoreTeam.class);
        when(lineScoreTeams.getHomeTeamScore()).thenReturn(homeLineScoreTeam);
        when(homeLineScoreTeam.getGoals()).thenReturn(HOME_GOALS);


        when(liveFeed.getLiveData().getBoxScore()).thenReturn(boxScore);
        when(liveFeed.getLiveData().getLineScore()).thenReturn(lineScore);
    }

    @Test
    public void testGetTileForGameJustGoals() throws Exception {
        Tile tile = TileUtil.getTileForGame(liveFeed, new HashSet<>(Collections.singletonList(StatType.GOALS)));

        assertEquals(3, tile.getRow1().getTextSegments().size());
        assertEquals(3, tile.getRow2().getTextSegments().size());
        assertEquals(2, tile.getRow3().getTextSegments().size());

        assertEquals("", tile.getRow1().getTextSegments().get(0));
        assertEquals(StatType.GOALS.getAbbreviation(), tile.getRow1().getTextSegments().get(1));
        assertEquals("PER", tile.getRow1().getTextSegments().get(2));

        assertEquals(AWAY_ABBREVIATION, tile.getRow2().getTextSegments().get(0));
        assertEquals(Integer.toString(AWAY_GOALS), tile.getRow2().getTextSegments().get(1));
        assertEquals(Integer.toString(PERIOD), tile.getRow2().getTextSegments().get(2));

        assertEquals(HOME_ABBREVIATION, tile.getRow3().getTextSegments().get(0));
        assertEquals(Integer.toString(HOME_GOALS), tile.getRow3().getTextSegments().get(1));
    }

    @Test
    public void testGetTileForGameGoalsAndShots() throws Exception {
        Tile tile = TileUtil.getTileForGame(liveFeed, new HashSet<>(Arrays.asList(StatType.GOALS, StatType.SHOTS)));

        assertEquals(4, tile.getRow1().getTextSegments().size());
        assertEquals(4, tile.getRow2().getTextSegments().size());
        assertEquals(3, tile.getRow3().getTextSegments().size());

        assertEquals("", tile.getRow1().getTextSegments().get(0));
        assertEquals(StatType.GOALS.getAbbreviation(), tile.getRow1().getTextSegments().get(1));
        assertEquals(StatType.SHOTS.getAbbreviation(), tile.getRow1().getTextSegments().get(2));
        assertEquals("PER", tile.getRow1().getTextSegments().get(3));

        assertEquals(AWAY_ABBREVIATION, tile.getRow2().getTextSegments().get(0));
        assertEquals(Integer.toString(AWAY_GOALS), tile.getRow2().getTextSegments().get(1));
        assertEquals(Integer.toString(AWAY_SHOTS), tile.getRow2().getTextSegments().get(2));
        assertEquals(Integer.toString(PERIOD), tile.getRow2().getTextSegments().get(3));

        assertEquals(HOME_ABBREVIATION, tile.getRow3().getTextSegments().get(0));
        assertEquals(Integer.toString(HOME_GOALS), tile.getRow3().getTextSegments().get(1));
        assertEquals(Integer.toString(HOME_SHOTS), tile.getRow3().getTextSegments().get(2));
    }

    @Test
    public void testGetTileForGameAllStats() throws Exception {
        Tile tile = TileUtil.getTileForGame(liveFeed, new HashSet<>(Arrays.asList(StatType.values())));

        assertEquals(StatType.values().length+2, tile.getRow1().getTextSegments().size());
        assertEquals(StatType.values().length+2, tile.getRow2().getTextSegments().size());
        assertEquals(StatType.values().length+1, tile.getRow3().getTextSegments().size());

        int i = 0;
        assertEquals("", tile.getRow1().getTextSegments().get(i++));
        assertEquals(StatType.GOALS.getAbbreviation(), tile.getRow1().getTextSegments().get(i++));
        assertEquals(StatType.SHOTS.getAbbreviation(), tile.getRow1().getTextSegments().get(i++));
        assertEquals(StatType.PENALTY_MINUTES.getAbbreviation(), tile.getRow1().getTextSegments().get(i++));
        assertEquals(StatType.HITS.getAbbreviation(), tile.getRow1().getTextSegments().get(i++));
        assertEquals(StatType.POWER_PLAY_PERCENTAGE.getAbbreviation(), tile.getRow1().getTextSegments().get(i++));
        assertEquals(StatType.POWER_PLAY_GOALS.getAbbreviation(), tile.getRow1().getTextSegments().get(i++));
        assertEquals(StatType.POWER_PLAY_OPPORTUNITIES.getAbbreviation(), tile.getRow1().getTextSegments().get(i++));
        assertEquals(StatType.FACE_OFF_PERCENTAGE.getAbbreviation(), tile.getRow1().getTextSegments().get(i++));
        assertEquals(StatType.BLOCKED_SHOTS.getAbbreviation(), tile.getRow1().getTextSegments().get(i++));
        assertEquals(StatType.TAKE_AWAYS.getAbbreviation(), tile.getRow1().getTextSegments().get(i++));
        assertEquals(StatType.GIVE_AWAYS.getAbbreviation(), tile.getRow1().getTextSegments().get(i++));
        assertEquals("PER", tile.getRow1().getTextSegments().get(i));

        i = 0;
        assertEquals(AWAY_ABBREVIATION, tile.getRow2().getTextSegments().get(i++));
        assertEquals(Integer.toString(AWAY_GOALS), tile.getRow2().getTextSegments().get(i++));
        assertEquals(Integer.toString(AWAY_SHOTS), tile.getRow2().getTextSegments().get(i++));
        assertEquals(Integer.toString(AWAY_PIM), tile.getRow2().getTextSegments().get(i++));
        assertEquals(Integer.toString(AWAY_HITS), tile.getRow2().getTextSegments().get(i++));
        assertEquals(AWAY_POWER_PLAY_PERCENTAGE, tile.getRow2().getTextSegments().get(i++));
        assertEquals(Integer.toString(AWAY_POWER_PLAY_GOALS), tile.getRow2().getTextSegments().get(i++));
        assertEquals(Integer.toString(AWAY_POWER_PLAY_OPPORTUNITIES), tile.getRow2().getTextSegments().get(i++));
        assertEquals(AWAY_FACEOFF_PERCENTAGE, tile.getRow2().getTextSegments().get(i++));
        assertEquals(Integer.toString(AWAY_BLOCKED_SHOTS), tile.getRow2().getTextSegments().get(i++));
        assertEquals(Integer.toString(AWAY_TAKE_AWAYS), tile.getRow2().getTextSegments().get(i++));
        assertEquals(Integer.toString(AWAY_GIVE_AWAYS), tile.getRow2().getTextSegments().get(i++));
        assertEquals(Integer.toString(PERIOD), tile.getRow2().getTextSegments().get(i));

        i = 0;
        assertEquals(HOME_ABBREVIATION, tile.getRow3().getTextSegments().get(i++));
        assertEquals(Integer.toString(HOME_GOALS), tile.getRow3().getTextSegments().get(i++));
        assertEquals(Integer.toString(HOME_SHOTS), tile.getRow3().getTextSegments().get(i++));
        assertEquals(Integer.toString(HOME_PIM), tile.getRow3().getTextSegments().get(i++));
        assertEquals(Integer.toString(HOME_HITS), tile.getRow3().getTextSegments().get(i++));
        assertEquals(HOME_POWER_PLAY_PERCENTAGE, tile.getRow3().getTextSegments().get(i++));
        assertEquals(Integer.toString(HOME_POWER_PLAY_GOALS), tile.getRow3().getTextSegments().get(i++));
        assertEquals(Integer.toString(HOME_POWER_PLAY_OPPORTUNITIES), tile.getRow3().getTextSegments().get(i++));
        assertEquals(HOME_FACEOFF_PERCENTAGE, tile.getRow3().getTextSegments().get(i++));
        assertEquals(Integer.toString(HOME_BLOCKED_SHOTS), tile.getRow3().getTextSegments().get(i++));
        assertEquals(Integer.toString(HOME_TAKE_AWAYS), tile.getRow3().getTextSegments().get(i++));
        assertEquals(Integer.toString(HOME_GIVE_AWAYS), tile.getRow3().getTextSegments().get(i));
    }

}