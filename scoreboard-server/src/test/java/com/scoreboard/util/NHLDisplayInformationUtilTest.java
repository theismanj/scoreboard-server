package com.scoreboard.util;

import com.scoreboard.pojo.NHLDisplayInformation;
import com.scoreboard.pojo.nhl.BoxScore;
import com.scoreboard.pojo.nhl.BoxScoreTeam;
import com.scoreboard.pojo.nhl.BoxScoreTeams;
import com.scoreboard.pojo.nhl.LineScore;
import com.scoreboard.pojo.nhl.LineScoreTeam;
import com.scoreboard.pojo.nhl.LineScoreTeams;
import com.scoreboard.pojo.nhl.LiveData;
import com.scoreboard.pojo.nhl.LiveFeedResponse;
import com.scoreboard.pojo.nhl.StatType;
import com.scoreboard.pojo.nhl.Team;
import com.scoreboard.pojo.nhl.TeamSkaterStats;
import com.scoreboard.pojo.nhl.TeamStats;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NHLDisplayInformationUtilTest {
    private static final String clientId = "1";
    private static final Integer gameId = 123456;
    private static final String AWAY_ABBREVIATION = "CHI";
    private static final String HOME_ABBREVIATION = "STL";
    private static final int AWAY_GOALS = 1;
    private static final int HOME_GOALS = 4;
    private static final int AWAY_SHOTS = 15;
    private static final int HOME_SHOTS = 50;
    private static final int AWAY_PIM = 2;
    private static final int HOME_PIM = 6;
    private static final int AWAY_HITS = 8;
    private static final int HOME_HITS = 18;
    private static final String AWAY_POWER_PLAY_PERCENTAGE = "0.0%";
    private static final String HOME_POWER_PLAY_PERCENTAGE = "100.0%";
    private static final int AWAY_POWER_PLAY_GOALS = 0;
    private static final int HOME_POWER_PLAY_GOALS = 1;
    private static final int AWAY_POWER_PLAY_OPPORTUNITIES = 3;
    private static final int HOME_POWER_PLAY_OPPORTUNITIES = 1;
    private static final String AWAY_FACEOFF_PERCENTAGE = "40%";
    private static final String HOME_FACEOFF_PERCENTAGE = "60%";
    private static final int AWAY_BLOCKED_SHOTS = 13;
    private static final int HOME_BLOCKED_SHOTS = 20;
    private static final int AWAY_TAKE_AWAYS = 7;
    private static final int HOME_TAKE_AWAYS = 10;
    private static final int AWAY_GIVE_AWAYS = 10;
    private static final int HOME_GIVE_AWAYS = 3;
    private static final int PERIOD = 3;
    private static final String PERIOD_ORDINAL = "3rd";

    private LiveFeedResponse liveFeed;
    private LineScore lineScore;
    private String nhlDisplayInformationId;

    @Before
    public void setUp() throws Exception {
        liveFeed = mock(LiveFeedResponse.class);

        LiveData liveData = mock(LiveData.class);
        when(liveFeed.getLiveData()).thenReturn(liveData);

        BoxScore boxScore = mock(BoxScore.class);
        when(liveData.getBoxScore()).thenReturn(boxScore);

        BoxScoreTeams boxScoreTeams = mock(BoxScoreTeams.class);
        when(boxScore.getTeams()).thenReturn(boxScoreTeams);

        BoxScoreTeam awayBoxScoreTeam = mock(BoxScoreTeam.class);
        when(boxScoreTeams.getAway()).thenReturn(awayBoxScoreTeam);

        BoxScoreTeam homeBoxScoreTeam = mock(BoxScoreTeam.class);
        when(boxScoreTeams.getHome()).thenReturn(homeBoxScoreTeam);

        Team awayTeam = mock(Team.class);
        when(awayBoxScoreTeam.getTeam()).thenReturn(awayTeam);
        when(awayTeam.getAbbreviation()).thenReturn(AWAY_ABBREVIATION);

        Team homeTeam = mock(Team.class);
        when(homeBoxScoreTeam.getTeam()).thenReturn(homeTeam);
        when(homeTeam.getAbbreviation()).thenReturn(HOME_ABBREVIATION);

        TeamStats awayTeamStats = mock(TeamStats.class);
        when(awayBoxScoreTeam.getTeamStats()).thenReturn(awayTeamStats);

        TeamSkaterStats awayTeamSkaterStats = mock(TeamSkaterStats.class);
        when(awayTeamStats.getTeamSkaterStats()).thenReturn(awayTeamSkaterStats);
        when(awayTeamSkaterStats.getShots()).thenReturn(AWAY_SHOTS);
        when(awayTeamSkaterStats.getPim()).thenReturn(AWAY_PIM);
        when(awayTeamSkaterStats.getHits()).thenReturn(AWAY_HITS);
        when(awayTeamSkaterStats.getPowerPlayPercentage()).thenReturn(AWAY_POWER_PLAY_PERCENTAGE);
        when(awayTeamSkaterStats.getPowerPlayGoals()).thenReturn(AWAY_POWER_PLAY_GOALS);
        when(awayTeamSkaterStats.getPowerPlayOpportunities()).thenReturn(AWAY_POWER_PLAY_OPPORTUNITIES);
        when(awayTeamSkaterStats.getFaceOffWinPercentage()).thenReturn(AWAY_FACEOFF_PERCENTAGE);
        when(awayTeamSkaterStats.getBlocked()).thenReturn(AWAY_BLOCKED_SHOTS);
        when(awayTeamSkaterStats.getTakeaways()).thenReturn(AWAY_TAKE_AWAYS);
        when(awayTeamSkaterStats.getGiveaways()).thenReturn(AWAY_GIVE_AWAYS);

        TeamStats homeTeamStats = mock(TeamStats.class);
        when(homeBoxScoreTeam.getTeamStats()).thenReturn(homeTeamStats);

        TeamSkaterStats homeTeamSkaterStats = mock(TeamSkaterStats.class);
        when(homeTeamStats.getTeamSkaterStats()).thenReturn(homeTeamSkaterStats);
        when(homeTeamSkaterStats.getShots()).thenReturn(HOME_SHOTS);
        when(homeTeamSkaterStats.getPim()).thenReturn(HOME_PIM);
        when(homeTeamSkaterStats.getHits()).thenReturn(HOME_HITS);
        when(homeTeamSkaterStats.getPowerPlayPercentage()).thenReturn(HOME_POWER_PLAY_PERCENTAGE);
        when(homeTeamSkaterStats.getPowerPlayGoals()).thenReturn(HOME_POWER_PLAY_GOALS);
        when(homeTeamSkaterStats.getPowerPlayOpportunities()).thenReturn(HOME_POWER_PLAY_OPPORTUNITIES);
        when(homeTeamSkaterStats.getFaceOffWinPercentage()).thenReturn(HOME_FACEOFF_PERCENTAGE);
        when(homeTeamSkaterStats.getBlocked()).thenReturn(HOME_BLOCKED_SHOTS);
        when(homeTeamSkaterStats.getTakeaways()).thenReturn(HOME_TAKE_AWAYS);
        when(homeTeamSkaterStats.getGiveaways()).thenReturn(HOME_GIVE_AWAYS);


        lineScore = mock(LineScore.class);
        when(liveData.getLineScore()).thenReturn(lineScore);
        when(lineScore.getCurrentPeriod()).thenReturn(PERIOD);

        LineScoreTeams lineScoreTeams = mock(LineScoreTeams.class);
        when(lineScore.getTeams()).thenReturn(lineScoreTeams);

        LineScoreTeam awayLineScoreTeam = mock(LineScoreTeam.class);
        when(lineScoreTeams.getAwayTeamScore()).thenReturn(awayLineScoreTeam);
        when(awayLineScoreTeam.getGoals()).thenReturn(AWAY_GOALS);

        LineScoreTeam homeLineScoreTeam = mock(LineScoreTeam.class);
        when(lineScoreTeams.getHomeTeamScore()).thenReturn(homeLineScoreTeam);
        when(homeLineScoreTeam.getGoals()).thenReturn(HOME_GOALS);


        when(liveFeed.getLiveData().getBoxScore()).thenReturn(boxScore);
        when(liveFeed.getLiveData().getLineScore()).thenReturn(lineScore);

        nhlDisplayInformationId = NHLDisplayInformationUtil.generateId(clientId, gameId);
    }

    @Test
    public void testGetDisplayInformationJustGoals() throws Exception {
        NHLDisplayInformation information = NHLDisplayInformationUtil.getDisplayInformationForGame(
                nhlDisplayInformationId, liveFeed.getLiveData(), new HashSet<>(Collections.singletonList(StatType.GOALS)));

        assertEquals(AWAY_ABBREVIATION, information.getAwayTeamAbbreviation());
        assertEquals(HOME_ABBREVIATION, information.getHomeTeamAbbreviation());
        assertEquals(Integer.toString(AWAY_GOALS), information.getAwayTeamScore());
        assertEquals(Integer.toString(HOME_GOALS), information.getHomeTeamScore());
        assertNull(information.getAwayTeamBlockedShots());
        assertNull(information.getHomeTeamBlockedShots());
        assertNull(information.getAwayTeamShots());
        assertNull(information.getHomeTeamShots());
        assertNull(information.getAwayTeamPowerPlayGoals());
        assertNull(information.getHomeTeamPowerPlayGoals());
        assertNull(information.getAwayTeamPowerPlayOpportinities());
        assertNull(information.getHomeTeamPowerPlayOpportinities());
        assertNull(information.getAwayTeamPowerPlayPercentage());
        assertNull(information.getHomeTeamPowerPlayPercentage());
        assertNull(information.getAwayTeamFaceOffPercentage());
        assertNull(information.getHomeTeamFaceOffPercentage());
        assertNull(information.getAwayTeamTakeAways());
        assertNull(information.getHomeTeamTakeAways());
        assertNull(information.getAwayTeamGiveAways());
        assertNull(information.getHomeTeamGiveAways());
        assertEquals(nhlDisplayInformationId, information.getId());
    }

    @Test
    public void testGetDisplayInformationAllStats() throws Exception {
        NHLDisplayInformation information = NHLDisplayInformationUtil.getDisplayInformationForGame(
                nhlDisplayInformationId, liveFeed.getLiveData(), new HashSet<>(Arrays.asList(StatType.values())));

        assertEquals(AWAY_ABBREVIATION, information.getAwayTeamAbbreviation());
        assertEquals(HOME_ABBREVIATION, information.getHomeTeamAbbreviation());
        assertEquals(Integer.toString(AWAY_GOALS), information.getAwayTeamScore());
        assertEquals(Integer.toString(HOME_GOALS), information.getHomeTeamScore());
        assertEquals(Integer.toString(AWAY_BLOCKED_SHOTS), information.getAwayTeamBlockedShots());
        assertEquals(Integer.toString(HOME_BLOCKED_SHOTS), information.getHomeTeamBlockedShots());
        assertEquals(Integer.toString(AWAY_SHOTS), information.getAwayTeamShots());
        assertEquals(Integer.toString(HOME_SHOTS), information.getHomeTeamShots());
        assertEquals(Integer.toString(AWAY_POWER_PLAY_GOALS), information.getAwayTeamPowerPlayGoals());
        assertEquals(Integer.toString(HOME_POWER_PLAY_GOALS), information.getHomeTeamPowerPlayGoals());
        assertEquals(Integer.toString(AWAY_POWER_PLAY_OPPORTUNITIES), information.getAwayTeamPowerPlayOpportinities());
        assertEquals(Integer.toString(HOME_POWER_PLAY_OPPORTUNITIES), information.getHomeTeamPowerPlayOpportinities());
        assertEquals(AWAY_POWER_PLAY_PERCENTAGE, information.getAwayTeamPowerPlayPercentage());
        assertEquals(HOME_POWER_PLAY_PERCENTAGE, information.getHomeTeamPowerPlayPercentage());
        assertEquals(AWAY_FACEOFF_PERCENTAGE, information.getAwayTeamFaceOffPercentage());
        assertEquals(HOME_FACEOFF_PERCENTAGE, information.getHomeTeamFaceOffPercentage());
        assertEquals(Integer.toString(AWAY_TAKE_AWAYS), information.getAwayTeamTakeAways());
        assertEquals(Integer.toString(HOME_TAKE_AWAYS), information.getHomeTeamTakeAways());
        assertEquals(Integer.toString(AWAY_GIVE_AWAYS), information.getAwayTeamGiveAways());
        assertEquals(Integer.toString(HOME_GIVE_AWAYS), information.getHomeTeamGiveAways());
        assertEquals(nhlDisplayInformationId, information.getId());
    }

    @Test
    public void testPeriodOrdinalExists() throws Exception {
        when(lineScore.getCurrentPeriodOrdinal()).thenReturn(PERIOD_ORDINAL);
        when(lineScore.getCurrentPeriodTimeRemaining()).thenReturn("2:32");
        NHLDisplayInformation information = NHLDisplayInformationUtil.getDisplayInformationForGame(
                nhlDisplayInformationId, liveFeed.getLiveData(), new HashSet<>(Collections.singletonList(StatType.GOALS)));

        assertEquals(PERIOD_ORDINAL, information.getPeriod());
    }

    @Test
    public void testPeriodOrdinalDoesNotExists() throws Exception {
        when(lineScore.getCurrentPeriodOrdinal()).thenReturn(null);
        NHLDisplayInformation information = NHLDisplayInformationUtil.getDisplayInformationForGame(
                nhlDisplayInformationId, liveFeed.getLiveData(), new HashSet<>(Collections.singletonList(StatType.GOALS)));

        assertEquals(Integer.toString(PERIOD), information.getPeriod());
    }

    @Test
    public void testFinal() throws Exception {
        when(lineScore.getCurrentPeriodOrdinal()).thenReturn("3rd");
        when(lineScore.getCurrentPeriodTimeRemaining()).thenReturn("Final");
        NHLDisplayInformation information = NHLDisplayInformationUtil.getDisplayInformationForGame(
                nhlDisplayInformationId, liveFeed.getLiveData(), new HashSet<>(Collections.singletonList(StatType.GOALS)));

        assertEquals("F", information.getPeriod());
    }

    @Test
    public void testGenerateId() throws Exception {
        assertEquals("1:2", NHLDisplayInformationUtil.generateId("1", 2));
    }
}