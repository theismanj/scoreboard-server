package com.scoreboard.bo;

import com.scoreboard.dao.NHLScoresDAO;
import com.scoreboard.pojo.nhl.Game;
import com.scoreboard.pojo.nhl.LiveFeedResponse;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NHLScoresBOTest {

    private NHLScoresDAO dao;
    private List<Game> games = new ArrayList<>();
    private NHLScoresBO bo;

    @Before
    public void setUp() {
        Game game1 = new Game();
        game1.setGamePk(1);
        game1.setLink("/1");
        games.add(game1);

        Game game2 = new Game();
        game2.setLink("/2/test");
        game2.setGamePk(2);
        games.add(game2);

        dao = mock(NHLScoresDAO.class);
        when(dao.getGamesForDate(any(DateTime.class))).thenReturn(games);
        when(dao.getLiveFeed(anyString())).thenReturn(mock(LiveFeedResponse.class));

        bo = new NHLScoresBO(dao);
    }

    @Test
    public void testGetTodaysGames() {
        List<Game> gamesReturned = bo.getTodaysGames();
        assertEquals(games.size(), gamesReturned.size());
        for (int i = 0; i < games.size(); i++) {
            assertEquals(games.get(i), gamesReturned.get(i));
        }

        List<Game> gamesReturned2 = bo.getTodaysGames();
        assertEquals(gamesReturned, gamesReturned2);
        verify(dao, times(1)).getGamesForDate(any(DateTime.class));
    }

    @Test
    public void testGetFeedsForGames() {
        List<LiveFeedResponse> response1 = bo.getFeedsForGames();
        List<LiveFeedResponse> response2 = bo.getFeedsForGames();

        assertEquals(response1.size(), response2.size());

        verify(dao, times(games.size())).getLiveFeed(anyString());
    }

    @Test
    public void testGetFeedForGame() {
        LiveFeedResponse response1 = bo.getFeedForGame(1);
        LiveFeedResponse response2 = bo.getFeedForGame(1);

        assertEquals(response1, response2);

        verify(dao, times(1)).getLiveFeed(anyString());
    }
}
