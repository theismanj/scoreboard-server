package com.scoreboard.bo;

import com.scoreboard.pojo.Mode;
import com.scoreboard.pojo.Settings;
import com.scoreboard.pojo.nhl.StatType;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;

public class SettingsBOTest {

    private static final String TEST_CLIENT_ID = "1";
    private SettingsBO bo;
    private Settings settings;

    @Before
    public void setUp() {
        bo = new SettingsBO();

        Map<Integer, Set<StatType>> gameIdToStatTypes = new HashMap<>();
        gameIdToStatTypes.put(1, new HashSet<>(Arrays.asList(StatType.values())));
        gameIdToStatTypes.put(2, new HashSet<>(Arrays.asList(StatType.GOALS, StatType.HITS)));

        settings = new Settings();
        settings.setMode(Mode.NHL_SCORES);
    }

    @Test
    public void testSetAndGetSettings() throws Exception {
        bo.setSettings(TEST_CLIENT_ID, settings);

        assertEquals(settings, bo.getSettings(TEST_CLIENT_ID));
    }

    @Test
    public void testClearSettings() throws Exception {
        bo.setSettings(TEST_CLIENT_ID, settings);
        bo.clearSettings(TEST_CLIENT_ID);

        assertEquals(SettingsBO.DEFAULT_SETTINGS, bo.getSettings(TEST_CLIENT_ID));
    }

    @Test
    public void testGetSettingsNeverSet() throws Exception {
        assertEquals(SettingsBO.DEFAULT_SETTINGS, bo.getSettings(TEST_CLIENT_ID));
    }

    @Test
    public void clearSettingsNeverSet() throws Exception {
        bo.clearSettings(TEST_CLIENT_ID);
    }

}