package com.scoreboard.service;

import com.scoreboard.bo.NHLScoresBO;
import com.scoreboard.bo.SettingsBO;
import com.scoreboard.pojo.AvailableStats;
import com.scoreboard.pojo.Mode;
import com.scoreboard.pojo.Settings;
import com.scoreboard.pojo.StatSetting;
import com.scoreboard.pojo.nhl.Game;
import com.scoreboard.pojo.nhl.StatType;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GamesServiceTest {
    private List<Game> games = new ArrayList<>();
    private GamesService gamesService;

    @Before
    public void setUp() throws Exception {
        Game game1 = new Game();
        game1.setGamePk(1);
        game1.setLink("/1");
        games.add(game1);

        Game game2 = new Game();
        game2.setLink("/2/test");
        game2.setGamePk(2);
        games.add(game2);

        NHLScoresBO scoresBO = mock(NHLScoresBO.class);
        when(scoresBO.getTodaysGames()).thenReturn(games);

        SettingsBO settingsBO = mock(SettingsBO.class);
        Map<Integer, Set<StatType>> settingsMap = new HashMap<>();
        settingsMap.put(1, new HashSet<>(Arrays.asList(StatType.GOALS, StatType.HITS, StatType.PENALTY_MINUTES)));
        Settings settings = new Settings(Mode.NHL_SCORES, settingsMap);
        when(settingsBO.getSettings(anyString())).thenReturn(settings);

        gamesService = new GamesService(scoresBO, settingsBO);
    }

    @Test
    public void testGetAvailableStats() throws Exception {
        Set<AvailableStats> response = gamesService.getAvailableStats("1");
        assertEquals(games.size(), response.size());

        for (AvailableStats availableStats : response) {
            if (availableStats.getGame().getId().equals(1)) {
                for (StatSetting statSetting : availableStats.getStatSettings()) {
                    switch (statSetting.getStatType()) {
                        case GOALS:
                        case HITS:
                        case PENALTY_MINUTES:
                            assertTrue(statSetting.isEnabled());
                            break;
                        default:
                            assertFalse(statSetting.isEnabled());
                    }
                }
            } else if (availableStats.getGame().getId().equals(2)) {
                for (StatSetting statSetting : availableStats.getStatSettings()) {
                    switch (statSetting.getStatType()) {
                        case GOALS:
                        case SHOTS:
                            assertTrue(statSetting.isEnabled());
                            break;
                        default:
                            assertFalse(statSetting.isEnabled());
                    }
                }
            } else {
                fail("Unexpected game found in response: " + availableStats.getGame().getId());
            }
        }
    }

}