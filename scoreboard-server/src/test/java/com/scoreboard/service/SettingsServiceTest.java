package com.scoreboard.service;

import com.scoreboard.bo.SettingsBO;
import com.scoreboard.pojo.Settings;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by jtheisman on 1/14/17.
 */
public class SettingsServiceTest {
    private static final String TEST_CLIENT_ID = "1";

    private SettingsService settingsService;
    private SettingsBO bo;

    @Before
    public void setUp() throws Exception {
        bo = mock(SettingsBO.class);
        settingsService = new SettingsService(bo);
    }

    @Test
    public void testSetSettings() throws Exception {
        Settings settings = new Settings();
        settingsService.setSettings(TEST_CLIENT_ID, settings);

        verify(bo, atLeastOnce()).setSettings(TEST_CLIENT_ID, settings);
    }

    @Test
    public void clearSettings() throws Exception {
        settingsService.clearSettings(TEST_CLIENT_ID);
        verify(bo, atLeastOnce()).clearSettings(TEST_CLIENT_ID);
    }

    @Test
    public void getSettings() throws Exception {
        settingsService.getSettings(TEST_CLIENT_ID);
        verify(bo, atLeastOnce()).getSettings(TEST_CLIENT_ID);
    }

}