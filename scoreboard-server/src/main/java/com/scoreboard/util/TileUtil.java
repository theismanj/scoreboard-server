package com.scoreboard.util;

import com.scoreboard.pojo.nhl.LiveFeedResponse;
import com.scoreboard.pojo.nhl.StatType;
import com.scoreboard.pojo.tile.Row;
import com.scoreboard.pojo.tile.Size;
import com.scoreboard.pojo.tile.Tile;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TileUtil {

    public static Tile getTileForGame(LiveFeedResponse liveFeed, Set<StatType> statsToInclude) {
        Tile tile = new Tile();

        List<List<String>> linesForTile = getLinesForGame(liveFeed, statsToInclude);

        tile.setRow1(new Row(linesForTile.get(0), "200, 200, 200", Size.SMALL));
        tile.setRow2(new Row(linesForTile.get(1), "200, 200, 200", Size.MEDUIM));
        tile.setRow3(new Row(linesForTile.get(2), "200, 200, 200", Size.MEDUIM));

        return tile;
    }

    private static List<List<String>> getLinesForGame(LiveFeedResponse liveFeed, Set<StatType> statsToInclude) {
        List<List<String>> lines = new ArrayList<>();
        List<String> line1 = new ArrayList<>();
        List<String> line2 = new ArrayList<>();
        List<String> line3 = new ArrayList<>();

        line1.add("");
        line2.add(liveFeed.getLiveData().getBoxScore().getTeams().getAway().getTeam().getAbbreviation());
        line3.add(liveFeed.getLiveData().getBoxScore().getTeams().getHome().getTeam().getAbbreviation());

        //checking for each stat to see if it is in stat type to ensure the stats are always in the order I choose.
        if (statsToInclude.contains(StatType.GOALS)) {
            line1.add(StatType.GOALS.getAbbreviation());
            line2.add(Integer.toString(liveFeed.getLiveData().getLineScore().getTeams().getAwayTeamScore().getGoals()));
            line3.add(Integer.toString(liveFeed.getLiveData().getLineScore().getTeams().getHomeTeamScore().getGoals()));
        }

        if (statsToInclude.contains(StatType.SHOTS)) {
            line1.add(StatType.SHOTS.getAbbreviation());
            line2.add(Integer.toString(liveFeed.getLiveData().getBoxScore().getTeams().getAway().getTeamStats().getTeamSkaterStats().getShots()));
            line3.add(Integer.toString(liveFeed.getLiveData().getBoxScore().getTeams().getHome().getTeamStats().getTeamSkaterStats().getShots()));
        }

        if (statsToInclude.contains(StatType.PENALTY_MINUTES)) {
            line1.add(StatType.PENALTY_MINUTES.getAbbreviation());
            line2.add(Integer.toString(liveFeed.getLiveData().getBoxScore().getTeams().getAway().getTeamStats().getTeamSkaterStats().getPim()));
            line3.add(Integer.toString(liveFeed.getLiveData().getBoxScore().getTeams().getHome().getTeamStats().getTeamSkaterStats().getPim()));
        }

        if (statsToInclude.contains(StatType.HITS)) {
            line1.add(StatType.HITS.getAbbreviation());
            line2.add(Integer.toString(liveFeed.getLiveData().getBoxScore().getTeams().getAway().getTeamStats().getTeamSkaterStats().getHits()));
            line3.add(Integer.toString(liveFeed.getLiveData().getBoxScore().getTeams().getHome().getTeamStats().getTeamSkaterStats().getHits()));
        }

        if (statsToInclude.contains(StatType.POWER_PLAY_PERCENTAGE)) {
            line1.add(StatType.POWER_PLAY_PERCENTAGE.getAbbreviation());
            line2.add(liveFeed.getLiveData().getBoxScore().getTeams().getAway().getTeamStats().getTeamSkaterStats().getPowerPlayPercentage());
            line3.add(liveFeed.getLiveData().getBoxScore().getTeams().getHome().getTeamStats().getTeamSkaterStats().getPowerPlayPercentage());
        }

        if (statsToInclude.contains(StatType.POWER_PLAY_GOALS)) {
            line1.add(StatType.POWER_PLAY_GOALS.getAbbreviation());
            line2.add(Integer.toString(liveFeed.getLiveData().getBoxScore().getTeams().getAway().getTeamStats().getTeamSkaterStats().getPowerPlayGoals()));
            line3.add(Integer.toString(liveFeed.getLiveData().getBoxScore().getTeams().getHome().getTeamStats().getTeamSkaterStats().getPowerPlayGoals()));
        }

        if (statsToInclude.contains(StatType.POWER_PLAY_OPPORTUNITIES)) {
            line1.add(StatType.POWER_PLAY_OPPORTUNITIES.getAbbreviation());
            line2.add(Integer.toString(liveFeed.getLiveData().getBoxScore().getTeams().getAway().getTeamStats().getTeamSkaterStats().getPowerPlayOpportunities()));
            line3.add(Integer.toString(liveFeed.getLiveData().getBoxScore().getTeams().getHome().getTeamStats().getTeamSkaterStats().getPowerPlayOpportunities()));
        }

        if (statsToInclude.contains(StatType.FACE_OFF_PERCENTAGE)) {
            line1.add(StatType.FACE_OFF_PERCENTAGE.getAbbreviation());
            line2.add(liveFeed.getLiveData().getBoxScore().getTeams().getAway().getTeamStats().getTeamSkaterStats().getFaceOffWinPercentage());
            line3.add(liveFeed.getLiveData().getBoxScore().getTeams().getHome().getTeamStats().getTeamSkaterStats().getFaceOffWinPercentage());
        }

        if (statsToInclude.contains(StatType.BLOCKED_SHOTS)) {
            line1.add(StatType.BLOCKED_SHOTS.getAbbreviation());
            line2.add(Integer.toString(liveFeed.getLiveData().getBoxScore().getTeams().getAway().getTeamStats().getTeamSkaterStats().getBlocked()));
            line3.add(Integer.toString(liveFeed.getLiveData().getBoxScore().getTeams().getHome().getTeamStats().getTeamSkaterStats().getBlocked()));
        }

        if (statsToInclude.contains(StatType.TAKE_AWAYS)) {
            line1.add(StatType.TAKE_AWAYS.getAbbreviation());
            line2.add(Integer.toString(liveFeed.getLiveData().getBoxScore().getTeams().getAway().getTeamStats().getTeamSkaterStats().getTakeaways()));
            line3.add(Integer.toString(liveFeed.getLiveData().getBoxScore().getTeams().getHome().getTeamStats().getTeamSkaterStats().getTakeaways()));
        }

        if (statsToInclude.contains(StatType.GIVE_AWAYS)) {
            line1.add(StatType.GIVE_AWAYS.getAbbreviation());
            line2.add(Integer.toString(liveFeed.getLiveData().getBoxScore().getTeams().getAway().getTeamStats().getTeamSkaterStats().getGiveaways()));
            line3.add(Integer.toString(liveFeed.getLiveData().getBoxScore().getTeams().getHome().getTeamStats().getTeamSkaterStats().getGiveaways()));
        }

        line1.add("PER");
        line2.add(Integer.toString(liveFeed.getLiveData().getLineScore().getCurrentPeriod()));

        lines.add(line1);
        lines.add(line2);
        lines.add(line3);
        return lines;
    }
}
