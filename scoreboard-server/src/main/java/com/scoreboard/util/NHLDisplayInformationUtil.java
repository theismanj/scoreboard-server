package com.scoreboard.util;

import com.scoreboard.pojo.NHLDisplayInformation;
import com.scoreboard.pojo.nhl.BoxScoreTeam;
import com.scoreboard.pojo.nhl.LiveData;
import com.scoreboard.pojo.nhl.StatType;
import com.scoreboard.pojo.nhl.TeamSkaterStats;

import java.util.Set;

public class NHLDisplayInformationUtil {

    public static NHLDisplayInformation getDisplayInformationForGame(String id, LiveData liveData, Set<StatType> statsToInclude) {
        NHLDisplayInformation displayInformation = new NHLDisplayInformation();
        displayInformation.setId(id);

        BoxScoreTeam awayBoxScore = liveData.getBoxScore().getTeams().getAway();
        TeamSkaterStats awayTeamSkaterStats = awayBoxScore.getTeamStats().getTeamSkaterStats();

        BoxScoreTeam homeBoxScore = liveData.getBoxScore().getTeams().getHome();
        TeamSkaterStats homeTeamSkaterStats = homeBoxScore.getTeamStats().getTeamSkaterStats();

        displayInformation.setAwayTeamAbbreviation(awayBoxScore.getTeam().getAbbreviation());
        displayInformation.setHomeTeamAbbreviation(homeBoxScore.getTeam().getAbbreviation());

        for (StatType statType : statsToInclude) {
            switch (statType) {
                case GOALS:
                    displayInformation.setAwayTeamScore(Integer.toString(liveData.getLineScore().getTeams().getAwayTeamScore().getGoals()));
                    displayInformation.setHomeTeamScore(Integer.toString(liveData.getLineScore().getTeams().getHomeTeamScore().getGoals()));
                    break;
                case SHOTS:
                    displayInformation.setAwayTeamShots(Integer.toString(awayTeamSkaterStats.getShots()));
                    displayInformation.setHomeTeamShots(Integer.toString(homeTeamSkaterStats.getShots()));
                    break;
                case PENALTY_MINUTES:
                    displayInformation.setAwayTeamPenaltyMinutes(Integer.toString(awayTeamSkaterStats.getPim()));
                    displayInformation.setHomeTeamPenaltyMinutes(Integer.toString(homeTeamSkaterStats.getPim()));
                    break;
                case HITS:
                    displayInformation.setAwayTeamHits(Integer.toString(awayTeamSkaterStats.getHits()));
                    displayInformation.setHomeTeamHits(Integer.toString(homeTeamSkaterStats.getHits()));
                    break;
                case POWER_PLAY_PERCENTAGE:
                    displayInformation.setAwayTeamPowerPlayPercentage(awayTeamSkaterStats.getPowerPlayPercentage());
                    displayInformation.setHomeTeamPowerPlayPercentage(homeTeamSkaterStats.getPowerPlayPercentage());
                    break;
                case POWER_PLAY_GOALS:
                    displayInformation.setAwayTeamPowerPlayGoals(Integer.toString(awayTeamSkaterStats.getPowerPlayGoals()));
                    displayInformation.setHomeTeamPowerPlayGoals(Integer.toString(homeTeamSkaterStats.getPowerPlayGoals()));
                    break;
                case POWER_PLAY_OPPORTUNITIES:
                    displayInformation.setAwayTeamPowerPlayOpportinities(Integer.toString(awayTeamSkaterStats.getPowerPlayOpportunities()));
                    displayInformation.setHomeTeamPowerPlayOpportinities(Integer.toString(homeTeamSkaterStats.getPowerPlayOpportunities()));
                    break;
                case FACE_OFF_PERCENTAGE:
                    displayInformation.setAwayTeamFaceOffPercentage(awayTeamSkaterStats.getFaceOffWinPercentage());
                    displayInformation.setHomeTeamFaceOffPercentage(homeTeamSkaterStats.getFaceOffWinPercentage());
                    break;
                case BLOCKED_SHOTS:
                    displayInformation.setAwayTeamBlockedShots(Integer.toString(awayTeamSkaterStats.getBlocked()));
                    displayInformation.setHomeTeamBlockedShots(Integer.toString(homeTeamSkaterStats.getBlocked()));
                    break;
                case TAKE_AWAYS:
                    displayInformation.setAwayTeamTakeAways(Integer.toString(awayTeamSkaterStats.getTakeaways()));
                    displayInformation.setHomeTeamTakeAways(Integer.toString(homeTeamSkaterStats.getTakeaways()));
                    break;
                case GIVE_AWAYS:
                    displayInformation.setAwayTeamGiveAways(Integer.toString(awayTeamSkaterStats.getGiveaways()));
                    displayInformation.setHomeTeamGiveAways(Integer.toString(homeTeamSkaterStats.getGiveaways()));
                    break;
            }
        }

        if (liveData.getLineScore().getCurrentPeriodOrdinal() == null) {
            displayInformation.setPeriod(Integer.toString(liveData.getLineScore().getCurrentPeriod()));
        } else if (liveData.getLineScore().getCurrentPeriodTimeRemaining().equalsIgnoreCase("final")) {
            displayInformation.setPeriod("F");
        } else {
            displayInformation.setPeriod(liveData.getLineScore().getCurrentPeriodOrdinal());
        }

        return displayInformation;
    }

    public static String generateId(String clientId, Integer gameId) {
        return clientId + ":" + gameId;
    }
}
