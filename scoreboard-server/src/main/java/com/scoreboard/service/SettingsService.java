package com.scoreboard.service;

import com.scoreboard.bo.SettingsBO;
import com.scoreboard.pojo.Settings;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

@Path("settings")
@Consumes(MediaType.APPLICATION_JSON)
public class SettingsService {

    private final SettingsBO settingsBO;

    @Inject
    SettingsService(SettingsBO settingsBO) {
        this.settingsBO = settingsBO;
    }

    @POST
    @Path("{clientId}")
    public void setSettings(@PathParam("clientId") String clientId,
                            Settings settings) {
        settingsBO.setSettings(clientId, settings);
    }

    @POST
    @Path("{clientId}/clear")
    public void clearSettings(@PathParam("clientId") String clientId) {
        settingsBO.clearSettings(clientId);
    }

    @GET
    @Path("{clientId}")
    public Settings getSettings(@PathParam("clientId") String clientId) {
        return settingsBO.getSettings(clientId);
    }
}
