package com.scoreboard.service;

import com.scoreboard.bo.NHLScoresBO;
import com.scoreboard.bo.SettingsBO;
import com.scoreboard.pojo.AvailableStats;
import com.scoreboard.pojo.NHLDisplayInformation;
import com.scoreboard.pojo.Settings;
import com.scoreboard.pojo.StatSetting;
import com.scoreboard.pojo.nhl.Game;
import com.scoreboard.pojo.nhl.StatType;
import com.scoreboard.util.NHLDisplayInformationUtil;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Path("games")
@Produces(MediaType.APPLICATION_JSON)
public class GamesService {

    private final NHLScoresBO nhlScoresBO;
    private final SettingsBO settingsBO;

    @Inject
    GamesService(NHLScoresBO nhlScoresBO,
                 SettingsBO settingsBO) {
        this.nhlScoresBO = nhlScoresBO;
        this.settingsBO = settingsBO;
    }

    @GET
    @Path("statTypes/{clientId}")
    public Set<AvailableStats> getAvailableStats(@PathParam("clientId") String clientId) {
        Set<AvailableStats> availableStatses = new HashSet<>();
        Set<StatType> allStatTypes = new HashSet<>(Arrays.asList(StatType.values()));

        List<Game> todaysGames = nhlScoresBO.getTodaysGames();
        Settings settings = clientId == null ? new Settings() : settingsBO.getSettings(clientId);

        for (Game game : todaysGames) {
            availableStatses.add(new AvailableStats(game,
                    getStatTypeToEnabled(settings.getStatTypesForGame(game.getId()), allStatTypes)));
        }

        return availableStatses;
    }

    @GET
    @Path("information/{clientId}")
    public List<NHLDisplayInformation> getGameInformationToDisplay(@PathParam("clientId") String clientId) {
        List<NHLDisplayInformation> displayInformations = new ArrayList<>();

        Settings settings = settingsBO.getSettings(clientId);

        for (Game game : nhlScoresBO.getTodaysGames()) {
            if (!settings.getStatTypesForGame(game.getId()).isEmpty()) {
                displayInformations.add(NHLDisplayInformationUtil
                        .getDisplayInformationForGame(
                                NHLDisplayInformationUtil.generateId(clientId, game.getId()),
                                nhlScoresBO.getFeedForGame(game.getId()).getLiveData(),
                                settings.getStatTypesForGame(game.getId())));
            }
        }

        return displayInformations;
    }

    private Set<StatSetting> getStatTypeToEnabled(Set<StatType> statTypesForGame, Set<StatType> allStatTypes) {
        Set<StatSetting> statSettings = new HashSet<>();

        for (StatType statType : allStatTypes) {
            statSettings.add(new StatSetting(statType, statTypesForGame.contains(statType)));
        }

        return statSettings;
    }
}
