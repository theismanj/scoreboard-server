package com.scoreboard.service;

import com.scoreboard.bo.NHLScoresBO;
import com.scoreboard.bo.SettingsBO;
import com.scoreboard.pojo.Settings;
import com.scoreboard.pojo.nhl.LiveFeedResponse;
import com.scoreboard.pojo.tile.Tile;
import com.scoreboard.util.TileUtil;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.HashSet;
import java.util.Set;

@Path("tile")
@Produces(MediaType.APPLICATION_JSON)
public class TileService {

    private final NHLScoresBO nhlScoresBO;
    private final SettingsBO settingsBO;

    @Inject
    TileService(NHLScoresBO nhlScoresBO,
                SettingsBO settingsBO) {
        this.nhlScoresBO = nhlScoresBO;
        this.settingsBO = settingsBO;
    }

    @GET
    @Path("{clientId}")
    public Set<Tile> getTiles(@PathParam("clientId") String clientId) {
        Set<Tile> tiles = new HashSet<>();

        Settings settings = settingsBO.getSettings(clientId);

        for (LiveFeedResponse liveFeed : nhlScoresBO.getFeedsForGames()) {
            if (!settings.getStatTypesForGame(liveFeed.getGameId()).isEmpty()) {
                tiles.add(TileUtil.getTileForGame(liveFeed, settings.getStatTypesForGame(liveFeed.getGameId())));
            }
        }

        return tiles;
    }


}
