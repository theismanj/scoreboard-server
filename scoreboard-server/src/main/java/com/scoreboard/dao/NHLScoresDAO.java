package com.scoreboard.dao;

import com.scoreboard.config.SpringConfig;
import com.scoreboard.pojo.nhl.Game;
import com.scoreboard.pojo.nhl.LiveFeedResponse;
import com.scoreboard.pojo.nhl.ScheduleResponse;
import com.sun.jersey.api.client.WebResource;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

@Component
public class NHLScoresDAO {

    private final WebResource webResource;

    @Inject
    NHLScoresDAO(@Named(SpringConfig.NHL) WebResource webResource) {
        this.webResource = webResource;
    }

    public List<Game> getGamesForDate(DateTime dateTime) {
        String date = dateTime.toString(DateTimeFormat.forPattern("yyyy-MM-dd"));

        ScheduleResponse response = webResource.path("api/v1/schedule")
                .queryParam("startDate", date)
                .queryParam("endDate", date)
                .queryParam("expand", "schedule.teams,schedule.linescore")
                .queryParam("site", "en_nhl")
                .header(HttpHeaders.USER_AGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36")
                .type(MediaType.APPLICATION_JSON)
                .get(ScheduleResponse.class);

        if (response.getDates().size() == 1) {
            return response.getDates().get(0).getGames();
        } else {
            return new ArrayList<>();
        }
    }

    public LiveFeedResponse getLiveFeed(String link) {
        return webResource.path(link)
                .accept(MediaType.APPLICATION_JSON).get(LiveFeedResponse.class);
    }
}
