package com.scoreboard;

import com.google.gson.Gson;
import com.scoreboard.pojo.Mode;
import com.scoreboard.pojo.Settings;
import com.scoreboard.pojo.nhl.StatType;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SettingsGenerator {

    public static void main(String[] args) {
        Settings settings = new Settings();
        settings.setMode(Mode.NHL_SCORES);

        Map<Integer, Set<StatType>> gameIdToStatTypes = new HashMap<>();
        gameIdToStatTypes.put(2016020651, new HashSet<>(Arrays.asList(StatType.GOALS, StatType.POWER_PLAY_PERCENTAGE)));
        gameIdToStatTypes.put(2016020640, new HashSet<StatType>());
        gameIdToStatTypes.put(2016020641, new HashSet<StatType>());
        gameIdToStatTypes.put(2016020642, new HashSet<StatType>());
        gameIdToStatTypes.put(2016020643, new HashSet<StatType>());
        gameIdToStatTypes.put(2016020644, new HashSet<StatType>());
        gameIdToStatTypes.put(2016020645, new HashSet<StatType>());
        gameIdToStatTypes.put(2016020646, new HashSet<StatType>());
        gameIdToStatTypes.put(2016020647, new HashSet<StatType>());
        gameIdToStatTypes.put(2016020648, new HashSet<StatType>());
        gameIdToStatTypes.put(2016020649, new HashSet<StatType>());
        gameIdToStatTypes.put(2016020650, new HashSet<StatType>());
        settings.setIdsToStatTypes(gameIdToStatTypes);

        System.out.println(new Gson().toJson(settings));
    }
}
