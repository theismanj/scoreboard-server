package com.scoreboard.config;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.inject.Named;
import javax.inject.Singleton;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.Set;

@Configuration
@ComponentScan(basePackages = "com.scoreboard")
public class SpringConfig {
    public static final String NHL = "NHL";
    public static Properties properties = null;

    @Bean
    @Singleton
    @Named(NHL)
    public WebResource getNHLWebResource() {
        ClientConfig config = new DefaultClientConfig();
        final Set<Class<?>> configClasses = config.getClasses();
        configClasses.add(GsonProvider.class);

        Client client = Client.create(config);

        return client.resource(getProperties().getProperty("nhl.url"));
    }

    public static Properties getProperties() {
        if (properties == null) {
            properties = new Properties();

            try {
                FileInputStream in = new FileInputStream("config.properties");
                try {
                    properties.load(in);
                } finally {
                    in.close();
                }
            } catch (Exception e) {
                System.err.println("Error getting properties file.");
            }
        }

        return properties;
    }
}
