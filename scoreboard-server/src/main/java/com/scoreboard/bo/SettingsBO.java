package com.scoreboard.bo;

import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;
import com.scoreboard.pojo.Mode;
import com.scoreboard.pojo.Settings;
import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

import javax.cache.Cache;
import javax.cache.CacheException;
import javax.cache.CacheFactory;
import javax.cache.CacheManager;
import java.util.HashMap;
import java.util.Map;

@Component
public class SettingsBO {
    static final Settings DEFAULT_SETTINGS = generateDefaultSettings();

    private Cache memcache;

    SettingsBO() {
        try {
            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
            Map<String, Object> properties = new HashMap<>();
            properties.put(GCacheFactory.EXPIRATION, new DateTime().plusDays(1).withHourOfDay(4).withMinuteOfHour(0).toDate());
            memcache = cacheFactory.createCache(properties);
        } catch (CacheException e) {
            System.err.println("Error creating cache.");
        }
    }

    public void setSettings(String clientId,
                            Settings settings) {
        memcache.put(clientId, settings);
    }

    public void clearSettings(String clientId) {
        memcache.remove(clientId);
    }

    public Settings getSettings(String clientId) {
        Settings settings = (Settings) memcache.get(clientId);

        if (settings == null) {
            return DEFAULT_SETTINGS;
        }

        return settings;

    }

    private static Settings generateDefaultSettings() {
        Settings defaultSettings = new Settings();
        defaultSettings.setMode(Mode.NHL_SCORES);
        return defaultSettings;
    }
}
