package com.scoreboard.bo;

import com.scoreboard.dao.NHLScoresDAO;
import com.scoreboard.pojo.nhl.Game;
import com.scoreboard.pojo.nhl.LiveFeedResponse;
import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class NHLScoresBO {
    //Length of time to cache the games in minutes.
    public static final int GAMES_CACHE_TIMEOUT = 60;
    public static final int LIVE_FEED_CACHE_TIMEOUT = 1;

    private final NHLScoresDAO dao;
    private List<Game> games;
    private DateTime gameLastUpdateTime = new DateTime(0L);
    private Map<Integer, LiveFeedResponse> gameIdToLiveFeed = new HashMap<>();
    private Map<Integer, DateTime> gameIdToLastLiveFeedFetchDateTime = new HashMap<>();

    @Inject
    public NHLScoresBO(NHLScoresDAO dao) {
        this.dao = dao;
    }

    public List<Game> getTodaysGames() {
        if (gameLastUpdateTime.isBefore(new DateTime().minusMinutes(GAMES_CACHE_TIMEOUT))) {
            //subtracting 8 hours so we get the scores for the previous day until 8am
            games = dao.getGamesForDate(new DateTime().minusHours(8));

            gameLastUpdateTime = new DateTime();
        }

        return games;
    }

    public List<LiveFeedResponse> getFeedsForGames() {
        for (Game game : getTodaysGames()) {
            if (!gameIdToLiveFeed.containsKey(game.getId()) ||
                    !gameIdToLastLiveFeedFetchDateTime.containsKey(game.getId()) ||
                    new DateTime().minusMinutes(LIVE_FEED_CACHE_TIMEOUT).isAfter(gameIdToLastLiveFeedFetchDateTime.get(game.getId()))) {
                gameIdToLiveFeed.put(game.getId(), dao.getLiveFeed(game.getLink()));
                gameIdToLastLiveFeedFetchDateTime.put(game.getId(), new DateTime());
            }
        }

        return new ArrayList<>(gameIdToLiveFeed.values());
    }

    public LiveFeedResponse getFeedForGame(int gameId) {
        if (!gameIdToLiveFeed.containsKey(gameId) ||
                !gameIdToLastLiveFeedFetchDateTime.containsKey(gameId) ||
                new DateTime().minusMinutes(LIVE_FEED_CACHE_TIMEOUT).isAfter(gameIdToLastLiveFeedFetchDateTime.get(gameId))) {
            gameIdToLiveFeed.put(gameId, dao.getLiveFeed(getLinkForGame(gameId)));
            gameIdToLastLiveFeedFetchDateTime.put(gameId, new DateTime());
        }

        return gameIdToLiveFeed.get(gameId);
    }

    private String getLinkForGame(int gameId) {
        //This is based on an assumption due to not being able to guarantee that game will be in the games list
        return "/api/v1/game/"+gameId+"/feed/live";
    }
}
