# README #

This is a simple Java server application that will run with the intention of being able to fetch NHL game information from an external source, cache it, and allow other applications to set which information gets displayed for certain clients. An example client application for using the NHL game information can be found at https://bitbucket.org/theismanj/nhl-marquee and an example client application for setting which information to be returned can be found at https://bitbucket.org/theismanj/scoreboard-controller.

### How do I get set up? ###

This can be deployed on any Java compatible server. I have chosen to deploy this on Google App Engine. Not included in this repo is config.properties which should be placed in the web directory of the scoreboard-server module. This should include the username and password you wish to secure the server with along with the url for the external API to fetch the game information. These parameters are "nhl.url", "trust.username", and "trust.password".