package com.scoreboard.pojo;

public class NHLDisplayInformation {
    private String id;

    private String awayTeamAbbreviation;
    private String homeTeamAbbreviation;

    private String awayTeamScore;
    private String homeTeamScore;

    private String awayTeamShots;
    private String homeTeamShots;

    private String awayTeamPenaltyMinutes;
    private String homeTeamPenaltyMinutes;

    private String awayTeamHits;
    private String homeTeamHits;

    private String awayTeamPowerPlayPercentage;
    private String homeTeamPowerPlayPercentage;

    private String awayTeamPowerPlayGoals;
    private String homeTeamPowerPlayGoals;

    private String awayTeamPowerPlayOpportinities;
    private String homeTeamPowerPlayOpportinities;

    private String awayTeamFaceOffPercentage;
    private String homeTeamFaceOffPercentage;

    private String awayTeamBlockedShots;
    private String homeTeamBlockedShots;

    private String awayTeamTakeAways;
    private String homeTeamTakeAways;

    private String awayTeamGiveAways;
    private String homeTeamGiveAways;

    private String period;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAwayTeamAbbreviation() {
        return awayTeamAbbreviation;
    }

    public void setAwayTeamAbbreviation(String awayTeamAbbreviation) {
        this.awayTeamAbbreviation = awayTeamAbbreviation;
    }

    public String getHomeTeamAbbreviation() {
        return homeTeamAbbreviation;
    }

    public void setHomeTeamAbbreviation(String homeTeamAbbreviation) {
        this.homeTeamAbbreviation = homeTeamAbbreviation;
    }

    public String getAwayTeamScore() {
        return awayTeamScore;
    }

    public void setAwayTeamScore(String awayTeamScore) {
        this.awayTeamScore = awayTeamScore;
    }

    public String getHomeTeamScore() {
        return homeTeamScore;
    }

    public void setHomeTeamScore(String homeTeamScore) {
        this.homeTeamScore = homeTeamScore;
    }

    public String getAwayTeamShots() {
        return awayTeamShots;
    }

    public void setAwayTeamShots(String awayTeamShots) {
        this.awayTeamShots = awayTeamShots;
    }

    public String getHomeTeamShots() {
        return homeTeamShots;
    }

    public void setHomeTeamShots(String homeTeamShots) {
        this.homeTeamShots = homeTeamShots;
    }

    public String getAwayTeamPenaltyMinutes() {
        return awayTeamPenaltyMinutes;
    }

    public void setAwayTeamPenaltyMinutes(String awayTeamPenaltyMinutes) {
        this.awayTeamPenaltyMinutes = awayTeamPenaltyMinutes;
    }

    public String getHomeTeamPenaltyMinutes() {
        return homeTeamPenaltyMinutes;
    }

    public void setHomeTeamPenaltyMinutes(String homeTeamPenaltyMinutes) {
        this.homeTeamPenaltyMinutes = homeTeamPenaltyMinutes;
    }

    public String getAwayTeamHits() {
        return awayTeamHits;
    }

    public void setAwayTeamHits(String awayTeamHits) {
        this.awayTeamHits = awayTeamHits;
    }

    public String getHomeTeamHits() {
        return homeTeamHits;
    }

    public void setHomeTeamHits(String homeTeamHits) {
        this.homeTeamHits = homeTeamHits;
    }

    public String getAwayTeamPowerPlayPercentage() {
        return awayTeamPowerPlayPercentage;
    }

    public void setAwayTeamPowerPlayPercentage(String awayTeamPowerPlayPercentage) {
        this.awayTeamPowerPlayPercentage = awayTeamPowerPlayPercentage;
    }

    public String getHomeTeamPowerPlayPercentage() {
        return homeTeamPowerPlayPercentage;
    }

    public void setHomeTeamPowerPlayPercentage(String homeTeamPowerPlayPercentage) {
        this.homeTeamPowerPlayPercentage = homeTeamPowerPlayPercentage;
    }

    public String getAwayTeamPowerPlayGoals() {
        return awayTeamPowerPlayGoals;
    }

    public void setAwayTeamPowerPlayGoals(String awayTeamPowerPlayGoals) {
        this.awayTeamPowerPlayGoals = awayTeamPowerPlayGoals;
    }

    public String getHomeTeamPowerPlayGoals() {
        return homeTeamPowerPlayGoals;
    }

    public void setHomeTeamPowerPlayGoals(String homeTeamPowerPlayGoals) {
        this.homeTeamPowerPlayGoals = homeTeamPowerPlayGoals;
    }

    public String getAwayTeamPowerPlayOpportinities() {
        return awayTeamPowerPlayOpportinities;
    }

    public void setAwayTeamPowerPlayOpportinities(String awayTeamPowerPlayOpportinities) {
        this.awayTeamPowerPlayOpportinities = awayTeamPowerPlayOpportinities;
    }

    public String getHomeTeamPowerPlayOpportinities() {
        return homeTeamPowerPlayOpportinities;
    }

    public void setHomeTeamPowerPlayOpportinities(String homeTeamPowerPlayOpportinities) {
        this.homeTeamPowerPlayOpportinities = homeTeamPowerPlayOpportinities;
    }

    public String getAwayTeamFaceOffPercentage() {
        return awayTeamFaceOffPercentage;
    }

    public void setAwayTeamFaceOffPercentage(String awayTeamFaceOffPercentage) {
        this.awayTeamFaceOffPercentage = awayTeamFaceOffPercentage;
    }

    public String getHomeTeamFaceOffPercentage() {
        return homeTeamFaceOffPercentage;
    }

    public void setHomeTeamFaceOffPercentage(String homeTeamFaceOffPercentage) {
        this.homeTeamFaceOffPercentage = homeTeamFaceOffPercentage;
    }

    public String getAwayTeamBlockedShots() {
        return awayTeamBlockedShots;
    }

    public void setAwayTeamBlockedShots(String awayTeamBlockedShots) {
        this.awayTeamBlockedShots = awayTeamBlockedShots;
    }

    public String getHomeTeamBlockedShots() {
        return homeTeamBlockedShots;
    }

    public void setHomeTeamBlockedShots(String homeTeamBlockedShots) {
        this.homeTeamBlockedShots = homeTeamBlockedShots;
    }

    public String getAwayTeamTakeAways() {
        return awayTeamTakeAways;
    }

    public void setAwayTeamTakeAways(String awayTeamTakeAways) {
        this.awayTeamTakeAways = awayTeamTakeAways;
    }

    public String getHomeTeamTakeAways() {
        return homeTeamTakeAways;
    }

    public void setHomeTeamTakeAways(String homeTeamTakeAways) {
        this.homeTeamTakeAways = homeTeamTakeAways;
    }

    public String getAwayTeamGiveAways() {
        return awayTeamGiveAways;
    }

    public void setAwayTeamGiveAways(String awayTeamGiveAways) {
        this.awayTeamGiveAways = awayTeamGiveAways;
    }

    public String getHomeTeamGiveAways() {
        return homeTeamGiveAways;
    }

    public void setHomeTeamGiveAways(String homeTeamGiveAways) {
        this.homeTeamGiveAways = homeTeamGiveAways;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }
}
