package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class LeagueRecord implements Serializable {
    private int wins;
    private int losses;
    private String type;

    LeagueRecord() {

    }

    public int getWins() {
        return wins;
    }

    public int getLosses() {
        return losses;
    }

    public String getType() {
        return type;
    }
}
