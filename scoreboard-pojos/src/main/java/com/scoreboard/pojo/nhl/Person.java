package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class Person implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String fullName;
    private String link;
    private String shootsCatches;
    private String rosterStatus;

    Person() {

    }

    public Integer getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public String getLink() {
        return link;
    }

    public String getShootsCatches() {
        return shootsCatches;
    }

    public String getRosterStatus() {
        return rosterStatus;
    }
}
