package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class Position implements Serializable {
    private static final long serialVersionUID = 1L;

    private String code;
    private String name;
    private String type;
    private String abbreviation;

    Position() {

    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
}
