package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class SkaterStats extends PlayerStats implements Serializable {
    private static final long serialVersionUID = 1L;

    private int hits;
    private int powerPlayGoals;
    private int powerPlayAssists;
    private int penaltyMinutes;
    private int faceoffWins;
    private int faceoffTaken;
    private int takeaways;
    private int giveaways;
    private int shortHandedGoals;
    private int shortHandedAssists;
    private int blocked;
    private int plusMinus;
    private String evenTimeOnIce;
    private String powerPlayTimeOnIce;
    private String shortHandedTimeOnIce;

    SkaterStats() {

    }

    public int getHits() {
        return hits;
    }

    public int getPowerPlayGoals() {
        return powerPlayGoals;
    }

    public int getPowerPlayAssists() {
        return powerPlayAssists;
    }

    public int getPenaltyMinutes() {
        return penaltyMinutes;
    }

    public int getFaceoffWins() {
        return faceoffWins;
    }

    public int getFaceoffTaken() {
        return faceoffTaken;
    }

    public int getTakeaways() {
        return takeaways;
    }

    public int getGiveaways() {
        return giveaways;
    }

    public int getShortHandedGoals() {
        return shortHandedGoals;
    }

    public int getShortHandedAssists() {
        return shortHandedAssists;
    }

    public int getBlocked() {
        return blocked;
    }

    public int getPlusMinus() {
        return plusMinus;
    }

    public String getEvenTimeOnIce() {
        return evenTimeOnIce;
    }

    public String getPowerPlayTimeOnIce() {
        return powerPlayTimeOnIce;
    }

    public String getShortHandedTimeOnIce() {
        return shortHandedTimeOnIce;
    }
}
