package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class LineScoreTeams implements Serializable {
    private LineScoreTeam away;
    private LineScoreTeam home;

    public LineScoreTeams() {

    }

    public LineScoreTeam getAwayTeamScore() {
        return away;
    }

    public LineScoreTeam getHomeTeamScore() {
        return home;
    }

    public void setAway(LineScoreTeam away) {
        this.away = away;
    }

    public void setHome(LineScoreTeam home) {
        this.home = home;
    }
}
