package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class ShootoutScore implements Serializable {
    private int scores;
    private int attempts;

    ShootoutScore() {

    }

    public int getScores() {
        return scores;
    }

    public int getAttempts() {
        return attempts;
    }
}
