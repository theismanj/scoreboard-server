package com.scoreboard.pojo.nhl;


import java.io.Serializable;
import java.util.Objects;

public class Game implements Serializable {
    private int gamePk;
    private String link;
    private String gameType;
    private String season;
    private String gameDate;
    private GameStatus status;
    private GameTeams teams;
    private LineScore linescore;
    private Venue venue;

    public Game() {

    }

    public Integer getId() {
        return gamePk;
    }

    public String getLink() {
        return link;
    }

    public String getGameType() {
        return gameType;
    }

    public String getSeason() {
        return season;
    }

    public String getGameDate() {
        return gameDate;
    }

    public GameStatus getStatus() {
        return status;
    }

    public GameTeams getTeams() {
        return teams;
    }

    public LineScore getLinescore() {
        return linescore;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setGamePk(int gamePk) {
        this.gamePk = gamePk;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public void setGameDate(String gameDate) {
        this.gameDate = gameDate;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public void setTeams(GameTeams teams) {
        this.teams = teams;
    }

    public void setLinescore(LineScore linescore) {
        this.linescore = linescore;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(gamePk);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Game) {
            return ((Game) obj).getId().equals(gamePk);
        }

        return false;
    }
}
