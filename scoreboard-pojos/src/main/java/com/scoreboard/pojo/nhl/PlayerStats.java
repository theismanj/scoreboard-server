package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class PlayerStats implements Serializable {
    private static final long serialVersionUID = 1L;

    private String timeOnIce;
    private int assists;
    private int goals;
    private int shots;

    PlayerStats() {

    }

    public String getTimeOnIce() {
        return timeOnIce;
    }

    public int getAssists() {
        return assists;
    }

    public int getGoals() {
        return goals;
    }

    public int getShots() {
        return shots;
    }
}
