package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class LineScoreTeam implements Serializable {
    private Team team;
    private int goals;
    private int shotsOnGoal;
    private boolean goaliePulled;
    private int numSkaters;
    private boolean powerPlay;

    public LineScoreTeam() {

    }

    public Team getTeam() {
        return team;
    }

    public int getGoals() {
        return goals;
    }

    public int getShotsOnGoal() {
        return shotsOnGoal;
    }

    public boolean isGoaliePulled() {
        return goaliePulled;
    }

    public int getNumSkaters() {
        return numSkaters;
    }

    public boolean isPowerPlay() {
        return powerPlay;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }

    public void setShotsOnGoal(int shotsOnGoal) {
        this.shotsOnGoal = shotsOnGoal;
    }

    public void setGoaliePulled(boolean goaliePulled) {
        this.goaliePulled = goaliePulled;
    }

    public void setNumSkaters(int numSkaters) {
        this.numSkaters = numSkaters;
    }

    public void setPowerPlay(boolean powerPlay) {
        this.powerPlay = powerPlay;
    }
}
