package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class Player  implements Serializable {
    private static final long serialVersionUID = 1L;

    private Person person;
    private String jerseyNumber;
    private Position position;
    private Stats stats;

    Player() {

    }

    public Person getPerson() {
        return person;
    }

    public String getJerseyNumber() {
        return jerseyNumber;
    }

    public Position getPosition() {
        return position;
    }

    public Stats getStats() {
        return stats;
    }
}
