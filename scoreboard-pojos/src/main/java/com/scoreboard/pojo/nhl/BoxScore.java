package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class BoxScore implements Serializable {
    private static final long serialVersionUID = 1L;

    private BoxScoreTeams teams;

    BoxScore() {

    }

    public BoxScoreTeams getTeams() {
        return teams;
    }
}
