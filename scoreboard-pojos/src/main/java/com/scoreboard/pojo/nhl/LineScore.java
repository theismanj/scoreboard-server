package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class LineScore implements Serializable {
    private int currentPeriod;
    private ShootoutInfo shootoutInfo;
    private LineScoreTeams teams;
    private String powerPlayStrength;
    private boolean hasShootout;
    private String currentPeriodOrdinal;
    private String currentPeriodTimeRemaining;

    public LineScore() {

    }

    public int getCurrentPeriod() {
        return currentPeriod;
    }

    public ShootoutInfo getShootoutInfo() {
        return shootoutInfo;
    }

    public LineScoreTeams getTeams() {
        return teams;
    }

    public String getPowerPlayStrength() {
        return powerPlayStrength;
    }

    public boolean isHasShootout() {
        return hasShootout;
    }

    public void setCurrentPeriod(int currentPeriod) {
        this.currentPeriod = currentPeriod;
    }

    public void setShootoutInfo(ShootoutInfo shootoutInfo) {
        this.shootoutInfo = shootoutInfo;
    }

    public void setTeams(LineScoreTeams teams) {
        this.teams = teams;
    }

    public void setPowerPlayStrength(String powerPlayStrength) {
        this.powerPlayStrength = powerPlayStrength;
    }

    public void setHasShootout(boolean hasShootout) {
        this.hasShootout = hasShootout;
    }

    public String getCurrentPeriodOrdinal() {
        return currentPeriodOrdinal;
    }

    public void setCurrentPeriodOrdinal(String currentPeriodOrdinal) {
        this.currentPeriodOrdinal = currentPeriodOrdinal;
    }

    public String getCurrentPeriodTimeRemaining() {
        return currentPeriodTimeRemaining;
    }

    public void setCurrentPeriodTimeRemaining(String currentPeriodTimeRemaining) {
        this.currentPeriodTimeRemaining = currentPeriodTimeRemaining;
    }
}
