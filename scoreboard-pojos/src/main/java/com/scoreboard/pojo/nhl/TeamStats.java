package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class TeamStats  implements Serializable {
    private static final long serialVersionUID = 1L;

    private TeamSkaterStats teamSkaterStats;

    TeamStats() {

    }

    public TeamSkaterStats getTeamSkaterStats() {
        return teamSkaterStats;
    }
}
