package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class GameStatus implements Serializable {
    private String abstractGameStatus;
    private String codedGameState;
    private String detailedState;
    private String statusCode;

    GameStatus() {

    }

    public String getAbstractGameStatus() {
        return abstractGameStatus;
    }

    public String getCodedGameState() {
        return codedGameState;
    }

    public String getDetailedState() {
        return detailedState;
    }

    public String getStatusCode() {
        return statusCode;
    }
}
