package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class GoalieStats extends PlayerStats implements Serializable {
    private static final long serialVersionUID = 1L;

    private int pim;
    private int saves;
    private int powerPlaySaves;
    private int shortHandedSaves;
    private int evenSaves;
    private int shortHandedShotsAgainst;
    private int evenShotsAgainst;
    private int powerPlayShotsAgainst;
    private String decision;

    GoalieStats() {

    }

    public int getPim() {
        return pim;
    }

    public int getSaves() {
        return saves;
    }

    public int getPowerPlaySaves() {
        return powerPlaySaves;
    }

    public int getShortHandedSaves() {
        return shortHandedSaves;
    }

    public int getEvenSaves() {
        return evenSaves;
    }

    public int getShortHandedShotsAgainst() {
        return shortHandedShotsAgainst;
    }

    public int getEvenShotsAgainst() {
        return evenShotsAgainst;
    }

    public int getPowerPlayShotsAgainst() {
        return powerPlayShotsAgainst;
    }

    public String getDecision() {
        return decision;
    }
}
