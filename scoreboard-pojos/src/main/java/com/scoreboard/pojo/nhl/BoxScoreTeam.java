package com.scoreboard.pojo.nhl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

public class BoxScoreTeam implements Serializable {
    private static final long serialVersionUID = 1L;

    private Team team;
    private TeamStats teamStats;
    private Map<String, Player> players;

    BoxScoreTeam() {}

    public Team getTeam() {
        return team;
    }

    public TeamStats getTeamStats() {
        return teamStats;
    }

    public Collection<Player> getPlayers() {
        return players.values();
    }
}
