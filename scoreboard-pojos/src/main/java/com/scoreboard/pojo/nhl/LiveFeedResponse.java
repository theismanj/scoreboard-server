package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class LiveFeedResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private LiveData liveData;
    private int gamePk;

    LiveFeedResponse() {

    }

    public LiveData getLiveData() {
        return liveData;
    }

    public int getGameId() {
        return gamePk;
    }
}
