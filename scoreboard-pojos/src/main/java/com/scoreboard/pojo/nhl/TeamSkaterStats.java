package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class TeamSkaterStats implements Serializable {
    private static final long serialVersionUID = 1L;

    private int pim;
    private int shots;
    private String powerPlayPercentage;
    private int powerPlayGoals;
    private int powerPlayOpportunities;
    private String faceOffWinPercentage;
    private int blocked;
    private int takeaways;
    private int giveaways;
    private int hits;

    TeamSkaterStats() {

    }

    public int getPim() {
        return pim;
    }

    public int getShots() {
        return shots;
    }

    public String getPowerPlayPercentage() {
        return powerPlayPercentage;
    }

    public int getPowerPlayGoals() {
        return powerPlayGoals;
    }

    public int getPowerPlayOpportunities() {
        return powerPlayOpportunities;
    }

    public String getFaceOffWinPercentage() {
        return faceOffWinPercentage;
    }

    public int getBlocked() {
        return blocked;
    }

    public int getTakeaways() {
        return takeaways;
    }

    public int getGiveaways() {
        return giveaways;
    }

    public int getHits() {
        return hits;
    }
}
