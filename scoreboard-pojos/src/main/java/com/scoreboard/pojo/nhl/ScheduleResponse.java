package com.scoreboard.pojo.nhl;

import java.io.Serializable;
import java.util.List;

public class ScheduleResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private String copyright;
    private int totalItems;
    private int wait;
    private List<DaySchedule> dates;

    public ScheduleResponse() {

    }

    public List<DaySchedule> getDates() {
        return dates;
    }

    public int getTotalItems() {
        return totalItems;
    }
}
