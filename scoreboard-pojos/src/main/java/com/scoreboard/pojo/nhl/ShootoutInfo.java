package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class ShootoutInfo implements Serializable {
    private ShootoutScore away;
    private ShootoutScore home;

    ShootoutInfo() {

    }

    public ShootoutScore getAwayShootoutInfo(){
        return away;
    }

    public ShootoutScore getHomeShootoutInfo(){
        return home;
    }
}
