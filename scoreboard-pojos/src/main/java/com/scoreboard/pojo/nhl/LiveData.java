package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class LiveData implements Serializable {
    private static final long serialVersionUID = 1L;

    private BoxScore boxscore;
    private LineScore linescore;

    LiveData() {

    }

    public BoxScore getBoxScore() {
        return boxscore;
    }

    public LineScore getLineScore() {
        return linescore;
    }
}
