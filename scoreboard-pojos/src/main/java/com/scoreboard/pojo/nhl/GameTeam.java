package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class GameTeam implements Serializable {
    private LeagueRecord leagueRecord;
    private int score;
    private Team team;

    public LeagueRecord getLeagueRecord() {
        return leagueRecord;
    }

    public int getScore() {
        return score;
    }

    public Team getTeam() {
        return team;
    }
}
