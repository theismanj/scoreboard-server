package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class Venue implements Serializable {
    private String name;

    Venue() {

    }

    public String getName() {
        return name;
    }
}
