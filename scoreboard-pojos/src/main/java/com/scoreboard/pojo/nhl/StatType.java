package com.scoreboard.pojo.nhl;

public enum StatType {
    GOALS("G", "Goals"),
    PENALTY_MINUTES("PIM", "Penalty Minutes"),
    SHOTS("S", "Shots"),
    POWER_PLAY_PERCENTAGE("PP%", "Power Play Percentage"),
    POWER_PLAY_GOALS("PPG", "Power Play Goals"),
    POWER_PLAY_OPPORTUNITIES("PPA", "Power Play Opportunities"),
    FACE_OFF_PERCENTAGE("FO%", "Face-off Percentage"),
    BLOCKED_SHOTS("B", "Blocked Shots"),
    TAKE_AWAYS("TA", "Take Aways"),
    GIVE_AWAYS("GA", "Give Aways"),
    HITS("H", "Hits");

    private final String abbreviation;
    private final String displayValue;

    StatType(String abbreviation,
             String displayValue) {
        this.abbreviation = abbreviation;
        this.displayValue = displayValue;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
