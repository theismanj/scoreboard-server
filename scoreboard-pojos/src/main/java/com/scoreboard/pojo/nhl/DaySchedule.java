package com.scoreboard.pojo.nhl;

import java.util.List;

public class DaySchedule {
    private String date;
    private int totalItems;
    private List<Game> games;

    DaySchedule() {
    }

    public List<Game> getGames() {
        return games;
    }
}
