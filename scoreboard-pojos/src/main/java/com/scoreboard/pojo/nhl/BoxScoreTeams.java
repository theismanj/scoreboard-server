package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class BoxScoreTeams implements Serializable {
    private static final long serialVersionUID = 1L;

    private BoxScoreTeam away;
    private BoxScoreTeam home;

    BoxScoreTeams() {

    }

    public BoxScoreTeam getAway() {
        return away;
    }

    public BoxScoreTeam getHome() {
        return home;
    }
}
