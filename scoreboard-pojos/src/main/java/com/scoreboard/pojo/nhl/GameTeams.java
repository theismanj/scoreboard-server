package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class GameTeams implements Serializable {
    private GameTeam away;
    private GameTeam home;

    GameTeams() {

    }

    public GameTeam getAwayTeam() {
        return away;
    }

    public GameTeam getHomeTeam() {
        return home;
    }
}
