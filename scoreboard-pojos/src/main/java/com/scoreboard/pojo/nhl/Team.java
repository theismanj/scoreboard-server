package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class Team implements Serializable {
    private int id;
    private String name;
    private String link;
    private Venue venue;
    private String abbreviation;
    private String teamName;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLink() {
        return link;
    }

    public Venue getVenue() {
        return venue;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public String getTeamName() {
        return teamName;
    }
}
