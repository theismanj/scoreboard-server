package com.scoreboard.pojo.nhl;

import java.io.Serializable;

public class Stats implements Serializable {
    private static final long serialVersionUID = 1L;

    private SkaterStats skaterStats;
    private GoalieStats goalieStats;

    Stats () {

    }

    public SkaterStats getSkaterStats() {
        return skaterStats;
    }

    public GoalieStats getGoalieStats() {
        return goalieStats;
    }
}
