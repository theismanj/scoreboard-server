package com.scoreboard.pojo;

import com.scoreboard.pojo.nhl.StatType;

import java.io.Serializable;

public class StatSetting implements Serializable {
    private StatType statType;
    private Boolean enabled;

    public StatSetting(StatType statType, Boolean enabled) {
        this.statType = statType;
        this.enabled = enabled;
    }

    public StatType getStatType() {
        return statType;
    }

    public void setStatType(StatType statType) {
        this.statType = statType;
    }

    public Boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
