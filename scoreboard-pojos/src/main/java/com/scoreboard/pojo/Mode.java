package com.scoreboard.pojo;

public enum Mode {
    NHL_SCORES,
    NCAAF_SCORES,
    WEATHER,
    STOCKS
}
