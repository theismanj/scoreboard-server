package com.scoreboard.pojo.tile;

import com.scoreboard.pojo.tile.Row;

public class Tile {
    private Row row1;
    private Row row2;
    private Row row3;

    public Tile() {

    }

    public Tile(Row row1, Row row2, Row row3) {
        this.row1 = row1;
        this.row2 = row2;
        this.row3 = row3;
    }

    public Row getRow1() {
        return row1;
    }

    public void setRow1(Row row1) {
        this.row1 = row1;
    }

    public Row getRow2() {
        return row2;
    }

    public void setRow2(Row row2) {
        this.row2 = row2;
    }

    public Row getRow3() {
        return row3;
    }

    public void setRow3(Row row3) {
        this.row3 = row3;
    }
}
