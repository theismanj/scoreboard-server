package com.scoreboard.pojo.tile;

import java.util.List;

public class Row {
    private List<String> textSegments;
    private String color;
    private Size size;

    public Row() {
    }

    public Row(List<String> textSegments, String color, Size size) {
        this.textSegments = textSegments;
        this.color = color;
        this.size = size;
    }

    public List<String> getTextSegments() {
        return textSegments;
    }

    public void setTextSegments(List<String> textSegments) {
        this.textSegments = textSegments;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }
}
