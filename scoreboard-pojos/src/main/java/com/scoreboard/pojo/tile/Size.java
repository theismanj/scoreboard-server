package com.scoreboard.pojo.tile;

public enum Size {
    SMALL,
    MEDUIM,
    LARGE,
    EXTRA_LARGE
}
