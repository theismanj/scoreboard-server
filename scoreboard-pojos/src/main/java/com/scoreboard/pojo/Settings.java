package com.scoreboard.pojo;

import com.scoreboard.pojo.nhl.StatType;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Settings implements Serializable {
    private Mode mode;
    private Map<Integer, Set<StatType>> idsToStatTypes = new HashMap<Integer, Set<StatType>>();

    public Settings() {

    }

    public Settings(Mode mode, Map<Integer, Set<StatType>> idsToStatTypes) {
        this.mode = mode;
        this.idsToStatTypes = idsToStatTypes;
    }

    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public void setIdsToStatTypes(Map<Integer, Set<StatType>> idsToStatTypes) {
        this.idsToStatTypes = idsToStatTypes;
    }

    public Set<StatType> getStatTypesForGame(Integer gameId) {
        if (idsToStatTypes.containsKey(gameId)) {
            return idsToStatTypes.get(gameId);
        } else if (Mode.NHL_SCORES.equals(mode)) {
            return new HashSet<>(Arrays.asList(StatType.GOALS, StatType.SHOTS));
        } else {
            return new HashSet<>();
        }
    }
}
