package com.scoreboard.pojo;

import com.scoreboard.pojo.nhl.Game;
import com.scoreboard.pojo.nhl.StatType;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

public class AvailableStats implements Serializable {
    private final Game game;
    private final Set<StatSetting> statSettings;

    public AvailableStats(Game game, Set<StatSetting> statSettings) {
        this.game = game;
        this.statSettings = statSettings;
    }

    public Game getGame() {
        return game;
    }

    public Set<StatSetting> getStatSettings() {
        return statSettings;
    }
}
